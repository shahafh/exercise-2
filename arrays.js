const arr = [1, 2, 3, 4];

const add2 = collection => collection.map(x => x + 2);
const onlyEvens = collection => collection.filter(x => x % 2 === 0);
const sum = collection => collection.reduce((acc, item) => acc + item, 0);

arr.forEach(item => {
  console.log(item);
});

const map = (collection, callback) => {
  const result = [];
  for (let ii = 0; ii < collection.length; ii++) {
    result.push(callback(collection[ii]));
  }
  return result;
};

const add2_2 = map(collection, item => item + 2);

const filter = (collection, callback) => {
  const result = [];
  for (let ii = 0; ii < collection.length; ii++) {
    if (callback(collection[ii])) {
      result.push(collection[ii]);
    }
  }
  return result;
};

const reduce = (collection, callback, initialValue) => {
  let result = initialValue || collection[0];
  const startIndex = initialValue ? 1 : 0;
  for (let ii = startIndex; ii < collection.length; ii++) {
    result = callback(result, collection[ii]);
  }
  return result;
};
