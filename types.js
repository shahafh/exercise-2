let x = 1; // block scoped value
const y = 2; // constant value
var z = 3; // blocks from passing code review

/*
1 -> Number, can be int | float
"a" | 'a' -> string
undefined -> an empty variable => let a; // a === undefined
NaN -> not a number => 1 + "a" === NaN
concat strings => "a" + 1 === "a1"
null => user defined empty
*/

const myFunc = (a, b) => a + b;
myFunc(1); // will return NaN, a === 1, b === undefined => this will not throw an error!!
console.log(myFunc(1));
