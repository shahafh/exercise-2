// const myObj = {
//   a: 1,
//   b: 2,
//   c: 3
// };

// --- constructor function --- //

// function oldStylePerson(name) {
//   this.name = name;
// }

// oldStylePerson.prototype.walk = function() {
//   console.log(`${this.name} is walking`);
// };

// const myOldStylePerson = new oldStylePerson("mike");
// myOldStylePerson.walk();

// --- simple object --- //
// const Person = name => ({
//   name
// });

// const myPerson = Person("joe"); // {name: 'joe' };

// --- the revealing module pattern -> only see what you return --- //
// const ComplicatedPerson = name => {
//   const something = 1;

//   const walk = () => {
//     console.log(`${name} is walking`);
//   };

//   const printSomething = () => console.log(something);

//   return {
//     name,
//     walk,
//     printSomething
//   };
// };

// const myComplicatedPerson = ComplicatedPerson("jim");
// myComplicatedPerson.walk();
// myComplicatedPerson.printSomething();

// --- OOP classes --- //
// class Creature {
//   constructor(type) {
//     this.type = type;
//   }
// }

// class OOPyPerson extends Creature {
//   constructor(name) {
//     super("Person");
//     this.name = name;
//     this.helperMethod = this.helperMethod.bind(this);
//   }

//   walk() {
//     this.helperMethod();
//   }

//   helperMethod() {
//     console.log(this.name + "is walking");
//   }
// }

// const myOOPyPerson = new OOPyPerson("jane");
// myOOPyPerson.walk();
